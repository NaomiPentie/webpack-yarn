$(document).ready(function () {
    $.getJSON('https://baconipsum.com/api/?type=all-meat&sentences=1&start-with-lorem=1',
            { 'type': 'meat-and-filler', 'start-with-lorem': '1', 'paras': '3' },
            function (baconGoodness) {
                if (baconGoodness && baconGoodness.length > 0) {
                    $("#idbacon").html('');
                    for (var i = 0; i < baconGoodness.length; i++)
                        $("#idbacon").append('<p>' + baconGoodness[i] + '</p>');
                    $("#idbacon").show();
                }
            });
 });